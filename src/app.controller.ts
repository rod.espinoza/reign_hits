import { Controller, Get, HttpService, Put, Query } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private httpService: HttpService,
  ) {}

  @Get('/hits')
  getLastHits() {
    return this.appService.getLastHits();
  }

  @Put('/hits/disable')
  disableHits(@Query('id') id) {
    return this.appService.disableByStoryUrl(id);
  }
}
