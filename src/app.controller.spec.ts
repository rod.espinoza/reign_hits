import { HttpService } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

describe('AppController', () => {
  let appController: AppController;
  let appService: AppService;
  let httpService: HttpService;
  beforeEach(() => {
    httpService = new HttpService();
    appService = new AppService();
    appController = new AppController(appService, httpService);
  });



  describe('getLastHits', () => {
    it('should return "{}"', () => {
      expect(appController.getLastHits()).toBe(Promise);
    })
  })

  describe('getBaseUrl', () => {
    it('Should return url with query timestamp ', () => {
      const time = new Date().getTime();
      const templateUrl = `https://hn.algolia.com/api/v1/search_by_date>${time}`
      expect(appController.getBaseUrl(time)).toBe(templateUrl));
  })
})

})