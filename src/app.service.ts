import { Injectable, Provider } from '@nestjs/common';
import { queueScheduler } from 'rxjs';
const { MongoClient } = require('mongodb');
const client = new MongoClient("mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass&ssl=false");



@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  async getLastHits(): Promise<any[]> {
    await client.connect()
    return new Promise(resolve => {
      client.db("reign").collection("hits").find({ deactivate: { $exists: false } }).sort({ created_at_i: -1 }).limit(10).toArray((error, result) => {
        if (error) throw error;
        resolve(result)
      })
    })
  }


  async getHitsInArray(values:any[]): Promise<any[]> {
    await client.connect();
    const objectsIds = values.map(v => v.objectID)
    return new Promise(resolve => {
      client.db('reign').collection('hits').find({ objectID: { $in: objectsIds } }).toArray((error, result) => {
        if (error) throw error;
        resolve(result)
      })
    })
  }

  async disableByStoryUrl(id: string) {
    await client.connect()
    console.log(id)
    const query = { objectID: id }
    var newvalues = { $set: { deactivate: true } };
    return new Promise(resolve => {
      client.db("reign").collection("hits").updateOne(query, newvalues, (error, res) => {
        if (error) throw error
        //console.log(res)
        resolve(res)
      })
    })
  }

   async insertMany(client, list) {
    await client.connect()
    return new Promise(resolve => {
      client.db("reign").collection("hits").insertMany(list, (error , res) => {
        if (error) throw error
        resolve(res);        
      });
      
    })
  } 


}
