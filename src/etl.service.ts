import { Controller, Get, HttpService, Injectable, Put, Query } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { AppService } from './app.service';
const { MongoClient } = require('mongodb');
const client = new MongoClient("mongodb://localhost:27017/?readPreference=primary&ssl=false");

@Injectable()
export class EtlService {
    constructor(private httpService: HttpService, private appService: AppService) {
    }

    @Cron(CronExpression.EVERY_10_SECONDS)
    async handleCron() {
        console.log('START LOADING')
        let maxTimeStamp = null;
        try {
            maxTimeStamp = await getMaxTimeStamp(client)
        } catch (error) {
            return;
        }
        const url = this.getBaseUrl(maxTimeStamp)
        await this.getAndInsertData(url)
        console.log('COMPLETE LOAD')
        async function getMaxTimeStamp(client) {
            await client.connect()
            return new Promise(resolve => {

                client.db("reign").collection("hits").find().sort({ created_at_i: -1 }).limit(1).toArray((error, res) => {
                    if (error) throw error;
                    if (res.length > 0) {
                        resolve(res[0]['created_at_i'])
                    }
                    else {
                        resolve(null)
                    }


                })
            })
        }

    }

    getBaseUrl(result) {
        const baseUrl = "https://hn.algolia.com/api/v1/search_by_date"
        const lowerOrMore = result ? '>' : '<'
        result = result ? result : new Date().getTime()

        const url = `${baseUrl}?query=nodejs&numericFilters=created_at_i${lowerOrMore}${result}`
        return url;
    }

    private async getAndInsertData(baseUrl: string) {
        const value = await this.httpService.get(baseUrl);
        value.subscribe(result => {
            let newHits = result.data.hits;
            if (newHits.length > 0) {
                let insertableHits = [];
                this.appService.getHitsInArray(newHits).then(bannedHits => {
                    insertableHits = this.preventDuplicate(newHits, bannedHits, insertableHits);
                    if (insertableHits.length === 0) return;
                    this.appService.insertMany(client, insertableHits);
                })
            }
        });
    }

    private preventDuplicate(newHits: any, bannedHits: any[], insertableHits: any[]) {
        for (var i = 0; i < newHits.length; i++) {
            let insert = true;
            for (var h = 0; h < bannedHits.length; i++) {

                if (bannedHits[i].objectID === newHits[h].objectID) {
                    insert = false;
                }
            }
            insert = this.areBothUrlsNull(newHits, i)
            if (insert) {
                // traspasar url
                newHits[i].url = newHits[i].url ? newHits[i].url : newHits[i].story_url;
                insertableHits.push(newHits[i]);
            }

        }
        // no hits repeated in array
        const cleanHits = [];
        for (let index = 0; index < insertableHits.length; index++) {
            let isSame= false
                for (let b = 0; b < cleanHits.length; b++) {
                    isSame = cleanHits[b].objectID === insertableHits[index].objectID  
                }
            if(!isSame){
              cleanHits.push(insertableHits[index])
            }
        }
        return cleanHits;
    }

    private areBothUrlsNull(newHits: any, i: number): boolean {
        return !((newHits[i].url === null) && (newHits[i].story_url === null));
    }

}